import { multiply, makeLowerCase } from "./HelperFunction";

test("multiply", ()=> {
    expect(multiply(2, 10)).toBe(20);
    expect(multiply(6, -2)).toBe(-12);
});

test("lowerCase", ()=> {
    expect(makeLowerCase("BeNjAMin")).toBe("benjamin");
});