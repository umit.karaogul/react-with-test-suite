import "./App.css";
import Button from "./Component/Button/Button.js";
import Search from "./Component/Search/Search.js";

function App() {
  return (
    <div className="App">
      <hr></hr>
      <Button />
      <hr></hr>
      <Search />
      <hr></hr>
    </div>
  );
}


export default App;